import Vue from 'vue'
import App from './App.vue'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import VueRouter from 'vue-router'
import GameMenu from './components/GameMenu.vue'
import GamePlay from './components/GamePlay.vue'
import GameOver from './components/GameOver.vue'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.config.productionTip = false
Vue.use(BootstrapVue)
Vue.use(IconsPlugin)
Vue.use(VueRouter)

const routes = [
  { path: '/menu', component: GameMenu },
  { path: '/play', name: 'gameplay', component: GamePlay, props: true },
  { path: '/gameover', name: 'gameover', component: GameOver, props: true}
]

const router = new VueRouter({
  routes,
  mode: 'history'
})

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
