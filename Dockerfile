FROM node:lts-alpine AS builder

WORKDIR /src
COPY package.json .
RUN npm install
COPY . .

RUN npm run build
# /src/dist

FROM node:lts-alpine AS production

ENV PORT 3000

WORKDIR /app
RUN npm install express morgan connect-history-api-fallback
COPY index.js .
COPY --from=builder /src/dist ./dist

EXPOSE 3000/tcp

CMD [ "node", "index.js" ]